package sp.ui;

import sp.dao.ConnectionManager;
import sp.utility.PomocnaKlasaDatumi;

public class ApplicationUI {

	public static void main(String[] args) {
		try {
			ConnectionManager.open();
		} catch (Exception e) {
			System.out.println("Greska pri ucitavanju baze podataka!");
			e.printStackTrace();
		}
		
			meni();	
			
		
		try {
			ConnectionManager.close();
		} catch (Exception e) {
			System.out.println("Greska u zatvaranju baze");
			e.printStackTrace();
		}

	}
	
	public static void menu () {
		System.out.println("================================");
		System.out.println("=======Svetsko Prvenstvo=======");
		System.out.println("\t1. - Rad sa drzavama");
		System.out.println("\t2. - Rad sa svetskim prvenstvima");
		
	}
	
	public static void meni () {
		int odluka = -1;
		while (odluka != 0) {
			menu();
			System.out.println("Opcija:");
			odluka = PomocnaKlasaDatumi.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				DrzavaUI.menu();
				break;
			case 2:
				PrvenstvoUI.menu();
				break;
				default:
					System.out.println("Nepostojeca opcija");
					break;
			}
		}
	}

}
