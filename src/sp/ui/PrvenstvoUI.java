package sp.ui;

import java.util.List;

import sp.dao.DrzaveDAO;
import sp.dao.PrvenstvaDAO;
import sp.model.Drzava;
import sp.model.SvetskoPrvenstvo;
import sp.utility.PomocnaKlasaDatumi;

public class PrvenstvoUI {
	
	public static void sortByNaziv () {
		try {
			List <SvetskoPrvenstvo> sp = PrvenstvaDAO.getAllSortedByNaziv();
			for (SvetskoPrvenstvo itSp : sp) {
				System.out.println(itSp);
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static void sortByYear () {
		try {
			List <SvetskoPrvenstvo> sp = PrvenstvaDAO.getAllSortedByYear();
			for (SvetskoPrvenstvo itSp : sp) {
				System.out.println(itSp);
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static void izmena () {
		System.out.println("Godina odrzavanja");
		int godina = PomocnaKlasaDatumi.ocitajCeoBroj();
		try {
			SvetskoPrvenstvo sp = PrvenstvaDAO.getByYear(godina);
			if (sp == null) {
				System.out.println("Ne postoji SP za datu godinu");
			}else {
				System.out.println("Unesite novi naziv");
				String naziv = PomocnaKlasaDatumi.ocitajTekst();
				
				Drzava drDomacin = null;
				while (drDomacin == null) {
					System.out.println("Prikaz svih drzava");
					DrzavaUI.ispisiSve();
					System.out.println();
					System.out.println("Unesite drzavu domacina");
					drDomacin = DrzavaUI.pronadji();				
				}
				Drzava drOsvajac = null;
				while (drOsvajac == null) {
					System.out.println("Prokaz svih drzava");
					DrzavaUI.ispisiSve();
					System.out.println();
					System.out.println("Unesite drzavu osvajaca");
					drOsvajac = DrzavaUI.pronadji();
				}
				
				sp.setNaziv(naziv);
				sp.setDomacin(drDomacin);
				sp.setOsvajac(drOsvajac);
				PrvenstvaDAO.update(sp);
				System.out.println("Uspesna izmena");
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static void unosPrvenstva () {
		SvetskoPrvenstvo sp = null;
		Drzava dom = null;
		Drzava pobednik = null;
		
		System.out.println("Unesite godinu prvenstva");
		int godina = PomocnaKlasaDatumi.ocitajCeoBroj();
		try {
			sp = PrvenstvaDAO.getByYear(godina);
			if (sp != null) {
				System.out.println("Vec postoji prvenstvo sa navedenom godinom!");
				return;
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom!");
			e.printStackTrace();
		}
		System.out.println("Unesite naziv prvenstva");
		String naziv = PomocnaKlasaDatumi.ocitajTekst();
		System.out.println("Unesite drzavu domacina");
		String domacin1 = PomocnaKlasaDatumi.ocitajTekst();
		try {
			dom = DrzaveDAO.getByNaziv(domacin1);
			if (dom == null) {
				System.out.println("Drzava sa navadenim nazivom ne postoji");
				return;
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
		System.out.println("Unesite drzavu osvajaca");
		String osvajac = PomocnaKlasaDatumi.ocitajTekst();
		try {
			pobednik = DrzaveDAO.getByNaziv(osvajac);
			if (pobednik == null) {
				System.out.println("Drzava sa navadenim nazivom ne postoji");
				return;
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
		sp = new SvetskoPrvenstvo (godina, naziv, dom, pobednik);
		try {
			PrvenstvaDAO.add(sp);
			System.out.println("Prvenstvo uspesno dodato");
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static void prikazSvih () {
		try {
			System.out.printf ("%5s %12s %15s %15s %15s \n", "ID", "GOD", "NAZIV", "DOMACIN", "POBEDNIK");
			List <SvetskoPrvenstvo> prvenstva = PrvenstvaDAO.getAll();
			for (SvetskoPrvenstvo itSp : prvenstva) {
				System.out.printf("%5s %12s %15s %15s %15s \n", itSp.getId(), itSp.getGodina(), itSp.getNaziv(), itSp.getDomacin().getNaziv(), itSp.getOsvajac().getNaziv());
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom!");
			e.printStackTrace();
		}
	}
	
	public static void ispisiMenu () {
		System.out.println("=====SVETSKA PRVENSTVA=====");
		System.out.println("\t1. - Prikaz svih prvenstava");
		System.out.println("\t2. - Unos svetskog prvenstva");
		System.out.println("\t3. - Izmena podataka o Svetskom prvenstvu");
		System.out.println("\t4. - Sortiranje");
	}
	
	public static void sortMeni () {
		System.out.println("Sortiranje prvenstava: ");
		System.out.println("\t1. - Po nazivu rastuce");
		System.out.println("\t2. Po godini odrzavanja rastuce");
	}
	
	public static void sort () {
		int odluka = -1;
		while (odluka != 0) {
			sortMeni();
			System.out.println("opcija: ");
			odluka = PomocnaKlasaDatumi.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				sortByNaziv ();
				break;
			case 2:
				sortByYear ();
				break;
				default:
					System.out.println("Nepostojeca opcija");
					break;
			}
		}
	}
	
	public static void menu () {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu ();
			System.out.println("Opcija: ");
			odluka = PomocnaKlasaDatumi.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				prikazSvih();
				break;
			case 2:
				unosPrvenstva ();
				break;
			case 3:
				izmena ();
				break;
			case 4:
				sort ();
				break;
				default: 
					System.out.println("Nepostojeca opcija");
					break;
			}
		}
	}

}
