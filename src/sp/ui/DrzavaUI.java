package sp.ui;

import java.util.List;

import sp.dao.DrzaveDAO;
import sp.model.Drzava;
import sp.utility.PomocnaKlasaDatumi;

public class DrzavaUI {
	
	public static void izmena () {
		Drzava dr = pronadji ();
		if (dr != null) {
			System.out.println("Unesite naziv drzave");
			String naziv = PomocnaKlasaDatumi.ocitajTekst();
			dr.setNaziv(naziv);			
			try {
				DrzaveDAO.update(dr);
				System.out.println("Drzava uspesno izmenjena");
			} catch (Exception e) {
				System.out.println("Greska u radu sa bazom");
				e.printStackTrace();
			}
		}
	}
	
	public static void unos () {
		System.out.println("Unesite naziv drzave");
		String naziv = PomocnaKlasaDatumi.ocitajTekst();
		try {
			Drzava dr = DrzaveDAO.getByNaziv(naziv);
			if (dr != null) {
				System.out.println("Drzava sa tim nazivom vec postoji");
			}else {
				dr = new Drzava (naziv);
				DrzaveDAO.add(dr);
				System.out.println("Drzava je uspesno dodata");
			}
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static void ispisiSve () {
		try {
			List <Drzava> sveDrzave = DrzaveDAO.getAll();
			System.out.printf("%-16s \n", "DRZAVA");
			for (Drzava itDrzava : sveDrzave) {
				System.out.printf("%-16s \n", itDrzava.getNaziv());
			}
			
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
	}
	
	public static Drzava pronadji () {
		System.out.println("Unesite naziv drzave");
		String naziv = PomocnaKlasaDatumi.ocitajTekst();
		try {
			Drzava dr = DrzaveDAO.getByNaziv(naziv);
			if (dr == null) {
				System.out.println("Ne postoji drzava sa tim nazivom");
			}
			return dr;
		} catch (Exception e) {
			System.out.println("Greska u radu sa bazom");
			e.printStackTrace();
		}
		return null;
	}
	
	public static void ispisiMeni () {
		System.out.println("====================");
		System.out.println("Rad sa drzavama - opcije:");
		System.out.println("\t1. - Ispis svih drzava");
		System.out.println("\t2. - Unos drzave");
		System.out.println("\t3. - Izmena drzave");
		System.out.println("\t4. - Statisticki prikaz drzave");
		System.out.println("\t \t ...");
		System.out.println("\t0. - Izlaz");
	}
	
	public static void menu () {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMeni ();
			System.out.println("Opcija: ");
			odluka = PomocnaKlasaDatumi.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSve();
				break;
			case 2:
				unos ();
				break;
			case 3:
				izmena ();
				break;
				default:
					System.out.println("Nepostojeca opcija");
			}
		}
	}
}
