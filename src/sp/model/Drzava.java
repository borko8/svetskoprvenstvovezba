package sp.model;

import java.util.ArrayList;
import java.util.List;

public class Drzava {
	
	protected final int id;
	protected String naziv;
	
	protected List <SvetskoPrvenstvo> listaOsvojenihPrvenstava = new ArrayList <> ();
	protected List <SvetskoPrvenstvo> listaOdrzavanihPrvenstava = new ArrayList <> ();
	
	public Drzava (int id, String naziv) {
		this.id = id;
		this.naziv = naziv;
	}
	
	public Drzava (String naziv) {
		this.id = 0;
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Drzava [id=" + id + ", naziv=" + naziv + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Drzava other = (Drzava) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<SvetskoPrvenstvo> getListaOsvojenihPrvenstava() {
		return listaOsvojenihPrvenstava;
	}

	public void setListaOsvojenihPrvenstava(List<SvetskoPrvenstvo> listaOsvojenihPrvenstava) {
		this.listaOsvojenihPrvenstava = listaOsvojenihPrvenstava;
	}

	public List<SvetskoPrvenstvo> getListaOdrzavanihPrvenstava() {
		return listaOdrzavanihPrvenstava;
	}

	public void setListaOdrzavanihPrvenstava(List<SvetskoPrvenstvo> listaOdrzavanihPrvenstava) {
		this.listaOdrzavanihPrvenstava = listaOdrzavanihPrvenstava;
	}

	public int getId() {
		return id;
	}
	
	
	
}
