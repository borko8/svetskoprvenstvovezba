package sp.model;

public class SvetskoPrvenstvo {
	
	protected final int id;
	protected int godina;
	protected String naziv;
	protected Drzava domacin;
	protected Drzava osvajac;
	
	
	public SvetskoPrvenstvo(int id, int godina, String naziv, Drzava domacin, Drzava osvajac) {
		super();
		this.id = id;
		this.godina = godina;
		this.naziv = naziv;
		this.domacin = domacin;
		this.osvajac = osvajac;
	}


	public SvetskoPrvenstvo(int godina, String naziv, Drzava domacin, Drzava osvajac) {
		super();
		this.id = 0;
		this.godina = godina;
		this.naziv = naziv;
		this.domacin = domacin;
		this.osvajac = osvajac;
	}


	@Override
	public String toString() {
		return "SvetskoPrvenstvo [id=" + id + ", godina=" + godina + ", naziv=" + naziv + ", domacin=" + domacin
				+ ", osvajac=" + osvajac + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + godina;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SvetskoPrvenstvo other = (SvetskoPrvenstvo) obj;
		if (godina != other.godina)
			return false;
		if (id != other.id)
			return false;
		return true;
	}


	public int getGodina() {
		return godina;
	}


	public void setGodina(int godina) {
		this.godina = godina;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public Drzava getDomacin() {
		return domacin;
	}


	public void setDomacin(Drzava domacin) {
		this.domacin = domacin;
	}


	public Drzava getOsvajac() {
		return osvajac;
	}


	public void setOsvajac(Drzava osvajac) {
		this.osvajac = osvajac;
	}


	public int getId() {
		return id;
	}
	
	
	
	
	
	
}
