package sp.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import sp.model.Drzava;
import sp.model.SvetskoPrvenstvo;



public class PrvenstvaDAO {
	
	public static boolean update(SvetskoPrvenstvo prvenstvo) throws Exception {
		PreparedStatement stmt = null;
		try {
			String sql = "UPDATE prvenstva SET naziv = ?, domacin = ?, osvajac = ? WHERE id = " + prvenstvo.getId();

			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			int index = 1;
			stmt.setString(index++, prvenstvo.getNaziv());
			stmt.setInt(index++, prvenstvo.getDomacin().getId());
			stmt.setInt(index++, prvenstvo.getOsvajac().getId());

			return stmt.executeUpdate() == 1;
		} finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}
		}
	
	public static boolean add (SvetskoPrvenstvo sp) throws Exception {
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO prvenstva (godina, naziv, domacin, osvajac) VALUES (?, ?, ?, ?)";
			stmt = ConnectionManager.getConnection().prepareStatement(sql);
			
			int index = 1;
			stmt.setInt(index++, sp.getGodina());
			stmt.setString(index++, sp.getNaziv());
			stmt.setInt(index++, sp.getDomacin().getId());
			stmt.setInt(index++, sp.getOsvajac().getId());
			
			return stmt.executeUpdate() == 1;
		}finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
		}
	}
	
	public static List <SvetskoPrvenstvo> getAll (String sortBy) throws Exception {
		List <SvetskoPrvenstvo> prvenstva = new ArrayList <> ();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, godina, naziv, domacin, osvajac FROM prvenstva " + sortBy;
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int godina = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int dom = rset.getInt(index++);
				int pob = rset.getInt(index++);
				
				Drzava domacin = DrzaveDAO.getById(dom);
				Drzava osvajac = DrzaveDAO.getById(pob);
				
				SvetskoPrvenstvo sp = new SvetskoPrvenstvo (id, godina, naziv, domacin, osvajac);
				prvenstva.add(sp);				
			}
		}finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}
		return prvenstva;
	}
	
	public static List <SvetskoPrvenstvo> getAllSortedByNaziv () throws Exception {
		return getAll ("ORDER BY naziv ASC");
	}
	
	public static List <SvetskoPrvenstvo> getAllSortedByYear () throws Exception {
		return getAll ("ORDER BY godina ASC");
	}
	
	public static List <SvetskoPrvenstvo> getAll () throws Exception {
		return getAllSortedByYear();
	}
	
	public static List <SvetskoPrvenstvo> getAllByDomacin (Drzava domacin) throws Exception {
		List<SvetskoPrvenstvo> prvenstva = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, godina, naziv, osvajac FROM prvenstva WHERE domacin =" + domacin.getId();
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int godina = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int pob = rset.getInt(index++);
				Drzava pobednik = DrzaveDAO.getById(pob);
				
				SvetskoPrvenstvo sp = new SvetskoPrvenstvo (id, godina, naziv, domacin, pobednik);
				prvenstva.add(sp);
			}
		}finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}
		return prvenstva;
	}
	
	public static SvetskoPrvenstvo getByYear (int year) throws Exception {
		SvetskoPrvenstvo sp = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String sql = "SELECT id, naziv, domacin, osvajac FROM prvenstva WHERE godina=" + year;
			stmt = ConnectionManager.getConnection().createStatement();
			rset = stmt.executeQuery(sql);
			
			if (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int domacin = rset.getInt(index++);
				int pobednik = rset.getInt(index++);
				Drzava dom = DrzaveDAO.getById(domacin);
				Drzava osvajac = DrzaveDAO.getById(pobednik);
				sp = new SvetskoPrvenstvo (id, year, naziv, dom, osvajac);
			}
			
			
		}finally {
			try {stmt.close();} catch (Exception ex) {ex.printStackTrace();}
			try {rset.close();} catch (Exception ex) {ex.printStackTrace();}
		}
		return sp;
	}
}
